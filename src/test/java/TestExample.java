import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.PsutiSearchPage;

import static enums.Texts.googleSearchingText;

public class TestExample extends TestCore {

    @BeforeTest
    public void signIn() {
        // Действия со стартовой точки, до теста
    }

    @AfterTest
    public void signOut() {
        // Действия после теста возвращающие тест к стартовой точке
    }

    // BeforeTest и AfterTest обеспечивают атомарность тестов

    @Test(description = "Тест строки поиска Google")
    public void psutiSearchingFieldTest() {
        PsutiSearchPage
                .initAndOpenPage()
                .assertThatSearchPageOpened()

                .enterTextIntoSearchingLine(googleSearchingText.getText())
                .pressStartSearchingButton();
        // TODO - другие действия и проверки
    }
}
