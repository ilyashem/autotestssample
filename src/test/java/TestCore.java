import helpers.ChromeDriverProvider;
import io.qameta.allure.selenide.AllureSelenide;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import static com.codeborne.selenide.Selenide.close;
import static com.codeborne.selenide.logevents.SelenideLogger.addListener;
import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Configuration.browser;
import static enums.Paths.targetUrl;

public class TestCore {

    @BeforeSuite
    public void globalSettings() {
        addListener("AllureSelenide", new AllureSelenide());

        baseUrl = targetUrl.getUrl();
        browser = ChromeDriverProvider.class.getName();
    }

    @AfterSuite
    public void tearDown() {
        close();
    }
}
