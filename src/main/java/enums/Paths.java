package enums;

public enum Paths {
    targetUrl("https://www.psuti.ru");

    private String url;

    Paths(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
