package enums;

public enum Texts {
    googleSearchingText("Test automation");

    private String text;

    Texts(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
