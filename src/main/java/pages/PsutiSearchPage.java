package pages;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.open;
import static enums.Paths.targetUrl;

public class PsutiSearchPage extends PageCore {

    private static final By FIELD_SEARCHING = By.xpath("//form[@role='search']//input[@title]");

    private static final By BUTTON_START_SEARCHING = By.xpath("//form[@role='search']//input[@type='submit']");

    public static PsutiSearchPage initAndOpenPage() {
        open(targetUrl.getUrl());
        return new PsutiSearchPage();
    }

    public PsutiSearchPage enterTextIntoSearchingLine(String newText) {
        super
                .removeAllTextOfField(FIELD_SEARCHING)
                .writeTextIntoField(FIELD_SEARCHING, newText);
        return this;
    }

    public SearchingResultsPage pressStartSearchingButton() {
        super
                .clickLeftMouseButton(BUTTON_START_SEARCHING);
        return SearchingResultsPage.initPage();
    }

    public PsutiSearchPage assertThatSearchPageOpened() {
        super
                .waitForElementAppeared(FIELD_SEARCHING)
                .waitForElementAppeared(BUTTON_START_SEARCHING);
        return this;
    }
}
