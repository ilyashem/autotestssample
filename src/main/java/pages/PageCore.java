package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

class PageCore {

    private static final int WAIT_TIMEOUT = 3;  // seconds

    protected PageCore clickLeftMouseButton(By elementLocator) {
        $(elementLocator).click();
        return this;
    }

    protected PageCore clickRightMouseButton(By elementLocator) {
        $(elementLocator).contextClick();
        return this;
    }

    protected  PageCore removeAllTextOfField(By fieldLocator) {
        $(fieldLocator).clear();
        return this;
    }

    protected PageCore writeTextIntoField(By fieldLocator, String newText) {
        $(fieldLocator).sendKeys(newText);
        return this;
    }

    protected PageCore waitForElementAppeared(By elementLocator, int seconds) {
        WebDriverWait wait = new WebDriverWait(getWebDriver(), seconds);
        wait.until(presenceOfElementLocated(elementLocator));
        return this;
    }

    protected PageCore waitForElementAppeared(By elementLocator) {
        waitForElementAppeared(elementLocator, WAIT_TIMEOUT);
        return this;
    }

    protected PageCore pressEnterKey(By elementLocator) {
        $(elementLocator).pressEnter();
        return this;
    }
}
